$(document).ready(function() {
  $(".btn-signin").click(signInClick);
  $(".btn-login").click(LogInClick);
});

function signInClick(event) {
  var email = $("#email-signin").val();
  var password = $("#senha-signin").val();
  createUser(email, password);
}

function createUser(email, password) {
  firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(function(response) {
      var userId = response.user.uid;
      redirectToTasks(userId);
    })
    .catch(function(error) {
      handleError(error);
    });
}

function LogInClick(event) {
  event.preventDefault();
  var email = $("#email-login").val();
  var password = $("#senha-login").val();
  signInUser(email, password);
}

function signInUser(email, password) {
  firebase.auth().signInWithEmailAndPassword(email, password)
    .then(function(response) {
      var userId = response.user.uid;
      redirectToTasks(userId);
    })
    .catch(function(error) {
      handleError(error)
    });
}

function handleError(error) {
  var errorMessage = error.message;
  alert(errorMessage);
}

function redirectToPosts(userId) {
  window.location = "posts.html?id=" + userId;
}

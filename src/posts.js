var database = firebase.database();
var USER_ID = window.location.search.match(/\?id=(.*)/)[1];

$(document).ready(function() {
  getPosts();
  $(".add-tasks").click(addTasksClick);
});

function getPosts() {
  database.ref("tasks/" + USER_ID).once('value')
    .then(function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        createListItem(childData.text, childKey)
      });
    });
 }







//ferramenta para pegar valores do GET

var query = location.search.slice(1);
console.log("query", query);
console.log("location", location);
var partes = query.split('&');
var data = {};
partes.forEach(function (parte) {
    var chaveValor = parte.split('=');
    var chave = chaveValor[0];
    var valor = chaveValor[1];
    data[chave] = valor;
});

//request para trazer postagens

var postagens;
var id = data["id"];

function trazPosts(){

    var xmlhttp = new XMLHttpRequest();
    var url = "https://testelaboratoria.firebaseio.com/users/"+id+"/postagens.json";
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            postagens = JSON.parse(this.responseText);  
            carregaPost();  
        }
    }
    xmlhttp.open ("GET", url, true);
    xmlhttp.send();
}

trazPosts();
function apagaPostagem(idPost){
    if (confirm('Quer apagar mesmo essa postagem?')) {
        url="https://testelaboratoria.firebaseio.com/users/"+id+"/postagens/"+idPost+".json";
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                trazPosts();
                return false;
            }
        }
        httpRequest.open ("DELETE", url, false);
        httpRequest.send();  
    }else{
        return false;
    }
} 

//request para enviar postagens

function fazPostagem(){
    url="https://testelaboratoria.firebaseio.com/users/"+id+"/postagens.json";
    var data = {};
    data.texto = document.getElementById("texto_post").value;
    data.privacidade = document.getElementById("privacidade").value;

    var json = JSON.stringify(data);                        
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            carregaPost();
            return false;
        }
    }
    httpRequest.open ("POST", url, false);
    httpRequest.setRequestHeader('Content-type','application/json; charset=utf-8');
    httpRequest.send(json);
}

function carregaPost(filtro=null){
    if(postagens){
        chaves =  Object.keys(postagens)
        var divPosts = document.getElementById("todos_posts");
        divPosts.innerHTML = "";
            
        for(var i = 0; i< chaves.length; i++){
            if(filtro==null || filtro==postagens[chaves[i]].privacidade){
                var divPost = document.createElement('div');
                divPost.id = chaves[i];
                divPost.className="post";
                document.body.appendChild(divPost);
                document.getElementById(chaves[i]).innerHTML = postagens[chaves[i]].texto
                +'<div id="div_'+chaves[i]+'" class="editar">'
                +'<textarea class="textarea_post" type="textarea" id="texto_post_'+chaves[i]+'" required>'+postagens[chaves[i]].texto+'</textarea>'
                +'<div class="opcoes_editar opcoes_privacidade"><select id="privacidade_'+chaves[i]+'"><option value="todos">Todos</option><option value="amigos">Amigos</option></select>'
                +'<button onclick="return editaPost(\''+chaves[i]+'\')">Publicar</button></div>'
                +'</div>'
                +'<div class="botoes_edicao"><button class="botao_edite" onclick="ocultaElemento(\'div_'+chaves[i]+'\')">editar</button>'
                +'<button class="botao_delete" onclick="apagaPostagem(\''+chaves[i]+'\');">deletar</button></div>';
                divPosts.appendChild(divPost);
            }
        }
  
    }

}

function editaPost(idPost){
   url="https://testelaboratoria.firebaseio.com/users/"+id+"/postagens/"+idPost+".json";
        var httpRequest = new XMLHttpRequest();

        data.texto = document.getElementById("texto_post_"+idPost).value;
        data.privacidade = document.getElementById("privacidade_"+idPost).value;
        var json = JSON.stringify(data);                        
        httpRequest.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                trazPosts();
                return false;
            }
        }
        httpRequest.open ("PUT", url, false);
        httpRequest.send(json);  
 }
function ocultaElemento(div) {
    var x = document.getElementById(div);
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
return false;
}